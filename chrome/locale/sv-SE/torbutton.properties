torbutton.circuit_display.internet = Internet
torbutton.circuit_display.ip_unknown = Okänd IP-adress.
torbutton.circuit_display.onion_site = Onion-webbplats
torbutton.circuit_display.this_browser = Denna webbläsare
torbutton.circuit_display.relay = Router
torbutton.circuit_display.tor_bridge = Bro
torbutton.circuit_display.unknown_country = Okänt land
torbutton.circuit_display.guard = Vakt
torbutton.circuit_display.guard_note = Din [Guard]-nod kanske inte ändras.
torbutton.circuit_display.learn_more = Lär dig mer
torbutton.circuit_display.click_to_copy = Klicka för att kopiera
torbutton.circuit_display.copied = Kopierat!
torbutton.content_sizer.margin_tooltip = Tor Browser lägger till denna marginal för att göra bredden och höjden på ditt fönster mindre tydlig. Därmed minskas möjligheten att spåra dig.
extensions.torbutton@torproject.org.description = Torbutton ger en knapp för att konfigurera Tor inställningar och snabbt och enkelt rensa privat surfdata.
torbutton.popup.external.title = Hämta en extern filtyp?
torbutton.popup.external.app = Tor Browser kan inte visa filen. Du behöver öppna den i ett annat program.\n\n
torbutton.popup.external.note = Vissa typer av filer kan få program att ansluta till internet utan att använda Tor.\n\n
torbutton.popup.external.suggest = Det är säkrast att öppna filer du har hämtat hem bara när du är offline, eller så kan du använda en Tor Live CD som till exempel Tails.\n
torbutton.popup.launch = Hämta fil
torbutton.popup.cancel = Avbryt
torbutton.popup.dontask = Hämta filer automatiskt i fortsättningen
torbutton.popup.no_newnym = Torbutton kan inte säkert ge dig en ny identitet. Det har inte tillgång till Tor Control Port.\n\nKör du Tor Browser Bundle?
torbutton.security_settings.menu.title = Säkerhetsinställningar
torbutton.title.prompt_torbrowser = Viktig information om Torbutton 
torbutton.popup.prompt_torbrowser = Torbutton fungerar annorlunda nu: Du kan inte slå av den längre.\n\nVi gjorde denna förändring eftersom det inte är säkert att använda Torbutton i en webbläsare som också används för icke-Tor surfning. Det var för många fel som vi inte kunde åtgärda på något annat sätt.\n\nOm du vill fortsätta använda Firefox normalt så bör du avinstallera Tor Browser och hämta Tor Browser Bundle. Tor Browser skyddar din integritet bättre än vanliga Firefox, även när Firefox används med Tor Button.\n\nFör att ta bort Torbutton, gå till Verktyg->Tillägg->Tillägg och klicka på Ta bort-knappen bredvid Torbutton.
torbutton.popup.short_torbrowser = Viktig information om Torbutton!\n\nTorbutton är nu alltid aktiverad.\n\nKlicka på Torbutton för mer information.

torbutton.popup.confirm_plugins = Insticksmoduler så som Flash kan äventyra din anonymitet och personliga integritet.\n\nDe kan också kringgå Tor för att avslöja var du befinner dig och vad din IP-adress är.\n\nÄr du säker på att du vill aktivera insticksmoduler?\n\n
torbutton.popup.never_ask_again = Fråga mig aldrig igen
torbutton.popup.confirm_newnym = Tor Browser kommer att stänga alla fönster och flikar. Alla webbplatssessioner kommer att gå förlorade.\n\nStarta om Tor Browser nu för att återställa din identitet?\n\n

torbutton.maximize_warning = Maximering av Tor Browser kan ge webbplatser möjlighet att upptäcka din skärmstorlek, vilket kan användas för att spåra dig. Vi rekommenderar att du lämnar Tor Browser-fönstret i dess standardstorlek.

# Canvas permission prompt. Strings are kept here for ease of translation.
canvas.siteprompt=Denna webbplats (%S) försökte extrahera HTML5-kanvasbilddata, som skulle kunna användas för att identifiera just din dator.\n\nSka Tor Browser tillåta denna webbplats att extrahera HTML5-kanvasbilddata?
canvas.notNow=Inte just nu
canvas.notNowAccessKey=N
canvas.allow=Tillåt i framtiden
canvas.allowAccessKey=A
canvas.never=Aldrig för denna sida (rekommenderas)
canvas.neverAccessKey=e

# Profile/startup error messages. Strings are kept here for ease of translation.
# LOCALIZATION NOTE: %S is the application name.
profileProblemTitle=%S Profilproblem
profileReadOnly=Du kan inte köra %S från ett skrivskyddat filsystem. Vänligen kopiera %S till en annan plats innan du försöker använda den.
profileReadOnlyMac=Du kan inte köra %S från ett skrivskyddat filsystem. Vänligen kopiera först %S till ditt skrivbord eller Applications-mappen innan du försöker använda den.
profileAccessDenied=%S har inte rättighet att använda profilen. Vänligen ändra dina rättigheter i filsystemet och försök igen!
profileMigrationFailed=Migrering av din befintliga %S profil misslyckades.\nNya inställningar kommer att användas.

# "Downloading update" string for the hamburger menu (see #28885).
# This string is kept here for ease of translation.
# LOCALIZATION NOTE: %S is the application name.
updateDownloadingPanelUILabel=Hämtar %S-uppdatering

# .Onion Page Info prompt.  Strings are kept here for ease of translation.
pageInfo_OnionEncryptionWithBitsAndProtocol=Anslutning krypterad (onion-tjänst, %1$S, %2$S bit nycklar, %3$S)
pageInfo_OnionEncryption=Anslutning krypterad (onion-tjänst)
pageInfo_OnionName=Onion-namn:

# Onion services strings.  Strings are kept here for ease of translation.
onionServices.learnMore=Lär dig mer
onionServices.errorPage.browser=Webbläsare
onionServices.errorPage.network=Nätverk
onionServices.errorPage.onionSite=Onion-webbplats
# LOCALIZATION NOTE: In the longDescription strings, %S will be replaced with
#                    an error code, e.g., 0xF3.
# Tor SOCKS error 0xF0:
onionServices.descNotFound.pageTitle=Problem med att läsa in onion-webbplats
onionServices.descNotFound.header=Onion-webbplats hittades inte
onionServices.descNotFound=Den mest troliga orsaken är att onion-webbplatsen är frånkopplad. Kontakta administratör för onion-webbplats.
onionServices.descNotFound.longDescription=Detaljer: %S — Den begärda beskrivningen för onion-tjänsten kan inte hittas på hashringen och därför kan tjänsten inte nås av klienten.
# Tor SOCKS error 0xF1:
onionServices.descInvalid.pageTitle=Problem med att läsa in onion-webbplats
onionServices.descInvalid.header=Onion-webbplats kan inte nås
onionServices.descInvalid=Onion-webbplatsen är onåbar på grund av ett internt fel.
onionServices.descInvalid.longDescription=Detaljer: %S — Den begärda beskrivningen för onion-tjänsten kan inte tolkas eller signaturvalidering misslyckades.
# Tor SOCKS error 0xF2:
onionServices.introFailed.pageTitle=Problem med att läsa in onion-webbplatsen
onionServices.introFailed.header=Onion-webbplats har kopplats från
onionServices.introFailed=Den mest troliga orsaken är att onion-webbplatsen är frånkopplad. Kontakta administratör för onion-webbplats.
onionServices.introFailed.longDescription=Detaljer: %S — Introduktion misslyckades, vilket innebär att beskrivningen hittades men tjänsten är inte längre ansluten till introduktionspunkten. Det är troligt att tjänsten har ändrat sin beskrivning eller att den inte körs.
# Tor SOCKS error 0xF3:
onionServices.rendezvousFailed.pageTitle=Problem med att läsa in onion-webbplats
onionServices.rendezvousFailed.header=Det går inte att ansluta till onion-webbplats
onionServices.rendezvousFailed=Onion-webbplatsen är upptagen eller Tor-nätverket är överbelastat. Försök igen senare.
onionServices.rendezvousFailed.longDescription=Detaljer: %S — Klienten misslyckades med att delta i tjänsten, vilket innebär att klienten inte kunde slutföra anslutningen.
# Tor SOCKS error 0xF4:
onionServices.clientAuthMissing.pageTitle=Autentisering krävs
onionServices.clientAuthMissing.header=Onion-webbplats kräver autentisering
onionServices.clientAuthMissing=Tillgång till onion-webbplats kräver en nyckel men ingen tillhandahölls.
onionServices.clientAuthMissing.longDescription=Detaljer: %S — Klienten hämtade den begärda beskrivningen för onion-tjänsten men kunde inte dekryptera dess innehåll eftersom information om klientautentisering saknas.
# Tor SOCKS error 0xF5:
onionServices.clientAuthIncorrect.pageTitle=Autentisering misslyckades
onionServices.clientAuthIncorrect.header=Autentisering av onion-webbplatsen misslyckades
onionServices.clientAuthIncorrect=Den medföljande nyckeln är felaktig eller har återkallats. Kontakta administratör för onion-webbplats.
onionServices.clientAuthIncorrect.longDescription=Detaljer: %S — Klienten kunde hämta den begärda beskrivningen för onion-tjänsten men kunde inte dekryptera dess innehåll med den tillhandahållna klientauktoriseringsinformationen. Detta kan innebära att åtkomst har återkallats.
# Tor SOCKS error 0xF6:
onionServices.badAddress.pageTitle=Problem med att läsa in onion-webbplats
onionServices.badAddress.header=Ogiltig onion-webbplatsadress
onionServices.badAddress=Den angivna adressen för onion-webbplatsen är ogiltig. Vänligen kontrollera att du har angett det korrekt.
onionServices.badAddress.longDescription=Detaljer: %S — Den tillhandahållna .onion-adressen är ogiltig. Det här felet returneras på grund av ett av följande skäl: adresskontrollsumman stämmer inte, den offentliga nyckeln ed25519 är ogiltig eller kodningen är ogiltig.
# Tor SOCKS error 0xF7:
onionServices.introTimedOut.pageTitle=Problem med att läsa in onion-webbplats
onionServices.introTimedOut.header=Tiden för skapandet av onion-webbplatskrets löpte ut
onionServices.introTimedOut=Det gick inte att ansluta till onion-webbplats, eventuellt på grund av en dålig nätverksanslutning.
onionServices.introTimedOut.longDescription=Detaljer: %S — Anslutningen till den begärda onion-tjänsten avbröts när man försökte bygga möteskretsen.
#
# LOCALIZATION NOTE: %S will be replaced with the .onion address.
onionServices.authPrompt.description2=%S begär att du autentisera.
onionServices.authPrompt.keyPlaceholder=Ange din privata nyckel för den här onion-tjänsten
onionServices.authPrompt.done=Färdig
onionServices.authPrompt.doneAccessKey=d
onionServices.authPrompt.invalidKey=Vänligen ange en giltig nyckel (52 base32-tecken eller 44 base64-tecken)
onionServices.authPrompt.failedToSetKey=Det går inte att konfigurera Tor med din nyckel
onionServices.authPreferences.header=Autentisering för onion-tjänster
onionServices.authPreferences.overview=Vissa onion-tjänster kräver att du identifierar dig med en nyckel (ett slags lösenord) innan du kan komma åt dem.
onionServices.authPreferences.savedKeys=Sparade nycklar…
onionServices.authPreferences.dialogTitle=Nycklar för onion-tjänster
onionServices.authPreferences.dialogIntro=Nycklar för följande onion-webbplatser lagras på din dator
onionServices.authPreferences.onionSite=Onion-webbplats
onionServices.authPreferences.onionKey=Förklaring
onionServices.authPreferences.remove=Ta bort
onionServices.authPreferences.removeAll=Ta bort alla
onionServices.authPreferences.failedToGetKeys=Det går inte att hämta nycklar från Tor
onionServices.authPreferences.failedToRemoveKey=Det går inte att ta bort nyckeln
onionServices.v2Deprecated.pageTitle=Varning för upphörande av V2 onion-webbplats
onionServices.v2Deprecated.header=Version 2 onion-webbplatser kommer snart att upphöra att gälla
onionServices.v2Deprecated=Denna onion-webbplats kan inte nås snart. Kontakta webbplatsadministratören och uppmuntra dem att uppgradera.
onionServices.v2Deprecated.longDescription=Tor avslutar sitt stöd för onion-tjänster av version 2 i början av juli 2021, och denna onion-webbplats kan inte längre nås på den här adressen. Om du är webbplatsadministratör kan du snart uppgradera till en onion-tjänst av version 3.
onionServices.v2Deprecated.tryAgain=Förstått
onionServices.v2Deprecated.tooltip=Denna onion-webbplats kommer inte vara nåbar snart

# Onion-Location strings.
onionLocation.alwaysPrioritize=Prioritera alltid onion-tjänster
onionLocation.alwaysPrioritizeAccessKey=a
onionLocation.notNow=Inte just nu
onionLocation.notNowAccessKey=n
onionLocation.description=Det finns en mer privat och säker version av denna webbplats via Tor-nätverket via onion-tjänster. Onion-tjänster hjälper webbplatsutgivare och deras besökare att besegra övervakning och censur.
onionLocation.tryThis=Prova onion-tjänster
onionLocation.onionAvailable=.onion tillgänglig
onionLocation.learnMore=Lär dig mer...
onionLocation.always=Alltid
onionLocation.askEverytime=Fråga varje gång
onionLocation.prioritizeOnionsDescription=Prioritera .onion-webbplatser när de är kända.
onionLocation.onionServicesTitle=Onion-tjänster

# LOCALIZATION NOTE: %S will be replaced with the cryptocurrency address.
cryptoSafetyPrompt.cryptoWarning=En kryptovaluta-adress (%S) har kopierats från en osäker webbplats. Den kunde ha ändrats.
cryptoSafetyPrompt.whatCanHeading=Vad kan du göra åt det?
cryptoSafetyPrompt.whatCanBody=Du kan försöka återansluta till en ny krets för att upprätta en säker anslutning eller acceptera risken och avvisa denna varning.
cryptoSafetyPrompt.learnMore=Lär dig mer
cryptoSafetyPrompt.primaryAction=Läs om fliken med en ny krets
cryptoSafetyPrompt.primaryActionAccessKey=R
cryptoSafetyPrompt.secondaryAction=Avvisa varning
cryptoSafetyPrompt.secondaryActionAccessKey=B
