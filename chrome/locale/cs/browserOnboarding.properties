# Copyright (c) 2019, The Tor Project, Inc.
# See LICENSE for licensing information.
# vim: set sw=2 sts=2 ts=8 et:

onboarding.tour-tor-welcome=Vítejte
onboarding.tour-tor-welcome.title=Jste připraveni.
onboarding.tour-tor-welcome.description=Prohlížeč Tor nabízí nejvyšší úroveň soukromí a bezpečnosti při prohlížení internetu. Ochrání vás před sledováním i cenzurou. Rychlý průvodce vám ukáže, jak ho používat.
onboarding.tour-tor-welcome.next-button=Přejít na Soukromí

onboarding.tour-tor-privacy=Soukromí
onboarding.tour-tor-privacy.title=Odežeňte sledovací a šmírující prvky.
onboarding.tour-tor-privacy.description=Prohlížeč Tor izoluje cookies a smaže vaši historii prohlížení hned po ukončení relace. Tyto úpravy vám zajistí maximální míru soukromí a bezpečnosti. Pro informace o ochraně přímo sítí Tor klepněte na „Síť Tor“.
onboarding.tour-tor-privacy.button=Přejít na síť Tor

onboarding.tour-tor-network=Síť Tor
onboarding.tour-tor-network.title=Používejte decentralizovanou síť.
onboarding.tour-tor-network.description=Prohlížeč Tor se k internetu připojuje skrze síť Tor sestavenou díky tisícům dobrovolníků po celém světě. Na rozdíl od VPN nemá síť Tor žádný centrální prvek, kterému musíte do rukou svěřit své soukromí.
onboarding.tour-tor-network.description-para2=NOVÉ: Nastavení sítě Tor, včetně možnosti vyžádat si most, pokud je Tor blokován, nyní najdete v Předvolbách.
onboarding.tour-tor-network.action-button=Přizpůsobte si nastavení sítě Tor
onboarding.tour-tor-network.button=Zobrazit řetězec

onboarding.tour-tor-circuit-display=Zobrazení řetězce
onboarding.tour-tor-circuit-display.title=Podívejte se na svou cestu.
onboarding.tour-tor-circuit-display.description=Pro každou doménu, kterou navštívíte, je vytvořeno šifrované spojení přes tři uzly Toru různě po světě. Žádná webová stránka neví, odkud se k ní připojujete. Nové spojení si můžete vyžádat klepnutím na „Nový řetězec sítí Tor k této stránce“ v zobrazení řetězce.
onboarding.tour-tor-circuit-display.button=Moje cesta
onboarding.tour-tor-circuit-display.next-button=Přejít na Zabezpečení

onboarding.tour-tor-security=Zabezpečení
onboarding.tour-tor-security.title=Určujte svůj prožitek.
onboarding.tour-tor-security.description=K dispozici máte rozšířená nastavení pro další zvýšení zabezpečení, např. blokování všech prvků, které mohou být potenciálně použity k útoku na váš počítač. Pro zobrazení různých možností a jejich fungování klepněte níže.
onboarding.tour-tor-security.description-suffix=Poznámka: Ve výchozím nastavení se NoScript ani HTTPS Everywhere na nástrojové liště nezobrazují, ale můžete si nastavení lišt změnit.
onboarding.tour-tor-security-level.button=Zobrazit nastavenou úroveň zabezpečení
onboarding.tour-tor-security-level.next-button=Přejít na pokročilé tipy

onboarding.tour-tor-expect-differences=Tipy
onboarding.tour-tor-expect-differences.title=Očekávejte rozdíly.
onboarding.tour-tor-expect-differences.description=Váš prožitek z prohlížení se díky všem bezpečnostním funkcím Toru může lišit. Stránky mohou být pomalejší a v závislosti na úrovni zabezpečení nemusí některé prvky fungovat. Můžete být také dotazováni na důkaz, že jste opravdu lidé a ne roboti.
onboarding.tour-tor-expect-differences.button=Často kladené otázky
onboarding.tour-tor-expect-differences.next-button=Přejít na služby Onion

onboarding.tour-tor-onion-services=Služby Onion
onboarding.tour-tor-onion-services.title=Extra ochrana
onboarding.tour-tor-onion-services.description=Služby Onion jsou stránky, které končí na .onion a poskytují extra ochranu pro autory i návštěvníky, včetně pojistek proti cenzuře. Umožňují každému poskytovat obsah a služby anonymně. Pro otevření anonymní onion stránky DuckDuckGo klepněte níže.
onboarding.tour-tor-onion-services.button=Navštívit Onion
onboarding.tour-tor-onion-services.next-button=Hotovo

onboarding.overlay-icon-tooltip-updated2=Podívejte se, co je nového\nv %S
onboarding.tour-tor-update.prefix-new=Nové
onboarding.tour-tor-update.prefix-updated=Aktualizováno

onboarding.tour-tor-toolbar=Panel nástrojů
onboarding.tour-tor-toolbar-update-9.0.title=Rozlučte se s tlačítkem Onion.
onboarding.tour-tor-toolbar-update-9.0.description=Funkce Toru jsme se snažíme plně integrovat přímo to prohlížeče Tor.
onboarding.tour-tor-toolbar-update-9.0.description-para2=Proto už není potřeba hledat informace o Toru pod vlastním tlačítkem, ale najdete je pod ikonou [i] přímo v adresním řádku. Možnost vyžádat si novou identitu se zase přesunula do hlavního tlačítka [≡].
onboarding.tour-tor-toolbar-update-9.0.button=Jak si vyžádat novou identitu
onboarding.tour-tor-toolbar-update-9.0.next-button=Přejít na síť Tor

# Circuit Display onboarding.
onboarding.tor-circuit-display.next=Další
onboarding.tor-circuit-display.done=Hotovo
onboarding.tor-circuit-display.one-of-three=1 ze 3
onboarding.tor-circuit-display.two-of-three=2 ze 3
onboarding.tor-circuit-display.three-of-three=3 ze 3

onboarding.tor-circuit-display.intro.title=Jak řetězce fungují?
onboarding.tor-circuit-display.intro.msg=Řetězce jsou sestaveny z náhodně vybraných uzlů, počítačů rozmístěných po světě a nastavených pro přeposílání toku v síti Tor. Řetězce vám zajišťují soukromí při prohlížení a spojení s onion službami.

onboarding.tor-circuit-display.diagram.title=Zobrazení řetězce
onboarding.tor-circuit-display.diagram.msg=Tento diagram ukazuje uzly tvořící řetězec k této stránce. Aby nebylo možné spojit vaše aktivity na různých stránkách, ke každé je vytvořen řetězec nový.

onboarding.tor-circuit-display.new-circuit.title=Potřebujete nový řetězec?
onboarding.tor-circuit-display.new-circuit.msg=Pokud se vám nedaří k nějaké stránce připojit, nebo se stránka nenačítá správně, můžete použít toto tlačítko k obnovení stránky skrze nový řetězec.
