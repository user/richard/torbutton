# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor-browser
-brand-short-name = Tor-browser
-brand-full-name = Tor-browser
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor Browser
-vendor-short-name = Tor-project
trademarkInfo = ‘Tor’ en het ‘Onion-logo’ zijn geregistreerde handelsmerken van Tor Project, Inc.
