# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor 浏览器
-brand-short-name = Tor 浏览器
-brand-full-name = Tor 浏览器
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor 浏览器
-vendor-short-name = Tor Project
trademarkInfo = “Tor”和“Onion Logo”是 Tor Project, Inc 的注册商标。
